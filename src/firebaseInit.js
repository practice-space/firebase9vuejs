// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDbfO7RF-RqB2aQNZlOhwKeLWBki1mRqJ0",
  authDomain: "review9-vuejs.firebaseapp.com",
  projectId: "review9-vuejs",
  storageBucket: "review9-vuejs.appspot.com",
  messagingSenderId: "621608293448",
  appId: "1:621608293448:web:af730e870f33541a83b47d"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export default app